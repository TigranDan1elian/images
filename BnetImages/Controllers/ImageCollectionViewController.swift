//
//  ImageViewController.swift
//  BnetImages
//
//  Created by Tigran Danielian on 22.06.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

private var itemsPerRow: CGFloat {
    let currentOrientation = UIDevice.current.orientation
    if currentOrientation == .landscapeLeft || currentOrientation == .landscapeRight {
        return 3
    } else {
        return 2
    }
}

class ImageCollectionViewController: UICollectionViewController {
    
    var loadMoreStatus = false
    private let sectionInsets = UIEdgeInsets(top: 30    ,
                                             left: 10,
                                             bottom: 30,
                                             right: 10)
    private lazy var networkManager = NetworkManager()
    private var imagesArray: [UIImage] = []
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let xib = UINib(nibName: "ImageCell", bundle: nil)
        collectionView!.register(xib, forCellWithReuseIdentifier: reuseIdentifier)
        let loadingXib = UINib(nibName: "LoadingViewCell", bundle: nil)
        collectionView!.register(loadingXib, forCellWithReuseIdentifier: "LoadingCell")
        networkManager.delegate = self
        networkManager.getImagesArray()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        guard let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        flowLayout.invalidateLayout()
    }
    
    
    
    // MARK:-- UICollectionViewDataSource
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return imagesArray.count
        } else if section == 1 {
            return 1
        }
        return 0
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ImageCollectionViewCell
            cell.imageView.image = imagesArray[indexPath.row]
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LoadingCell", for: indexPath) as! LoadingViewCell
            cell.activityIndicator.startAnimating()
            return cell
            
        }
        
    }
    
    
    // MARK:-- UICollectionViewDelegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        // print("offsetY: \(offsetY), contentHeight: \(contentHeight)")
        
        switch UIDevice.current.orientation {
        case .landscapeRight, .landscapeLeft:
            if offsetY > (contentHeight - scrollView.frame.height) && !loadMoreStatus {
                loadMore()
            }
        default:
            if offsetY > (contentHeight - scrollView.frame.height * 4) && !loadMoreStatus {
                
                loadMore()
                
            }
        }
    }
    
    func loadMore() {
        loadMoreStatus = true
        // collectionView.reloadSections(IndexSet(integer: 1))
        networkManager.getImagesArray()
    }
    
}

extension ImageCollectionViewController: NetworkManagerDelegate {
    func didUpdate(images: [UIImage]) {
        DispatchQueue.main.async {
            self.imagesArray = images
            self.loadMoreStatus = false
            self.collectionView.reloadData()
        }
        
    }
    
    
    func didFailWithError(error: Error) {
        print(error)
    }
    
}

extension ImageCollectionViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.top * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        
        
        return CGSize(width: widthPerItem, height: widthPerItem)
        
        
    }
    
}
