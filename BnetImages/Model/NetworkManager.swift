//
//  NetworkManager.swift
//  BnetImages
//
//  Created by Tigran Danielian on 23.06.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import UIKit

protocol NetworkManagerDelegate {
    func didUpdate(images: [UIImage])
    func didFailWithError(error: Error)
}

class NetworkManager {
    
    enum NetworkError: Error {
        case unknown
        case remote(String)
    }
    
    var delegate: NetworkManagerDelegate?
    private var imagesStringArray: [String] = []
    private var imgs: [UIImage] = []
    private var numberOfImages = 10
    
    
    let baseUrl = "https://bnet.i-partner.ru/projects/calc/networkimages/"
    
    
    func getImagesArray(){
        if let arrayUrl = URL(string: baseUrl + "?a=getRandom&count=\(numberOfImages)") {
            let session = URLSession.shared
            let getArrayDataTask = session.dataTask(with: arrayUrl) { (data, response, error) in
                guard let data = data else {
                    self.delegate?.didFailWithError(error: NetworkError.remote("Data error"))
                    return
                }
                do {
                    let array = try JSONSerialization.jsonObject(with: data, options: []) as! [String]
                    for path in array{
                        let corrPath = self.correctPath(path: path)
                        self.getImage(path: self.baseUrl + corrPath)
                         self.delegate?.didUpdate(images: self.imgs)
                    }
                   
                    self.numberOfImages += 10
                    
                } catch {
                    self.delegate?.didFailWithError(error: NetworkError.remote("Data error"))
                    print("Data error")
                }
                
            }
            getArrayDataTask.resume()
            
        }
    }
    
    private func correctPath(path: String) -> String {
        var correctPath = path
        correctPath.removeFirst()
        return correctPath
    }
    
    private func getImage(path: String) {
        guard let imageData = try? Data(contentsOf: URL(string: path)!) else {
            print("bad data")
            return
        }
        DispatchQueue.main.async {
            let image = UIImage(data: imageData)
            self.imgs.append(image!)
        }
        
        
    }
    
}


