//
//  LoadingViewCell.swift
//  BnetImages
//
//  Created by Tigran Danielian on 26.06.2020.
//  Copyright © 2020 Tigran Danielian. All rights reserved.
//

import UIKit

class LoadingViewCell: UICollectionViewCell {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
